# `bazelisk`

> A component for running Bazel builds

## Getting Started

Add the CI component to the `.gitlab-ci.yml`:

```yaml
include:
  - component: "${CI_SERVER_HOST}/ci/component/bazelisk/bazelisk@<version>"
```

Add a job that extends the `.bazelisk` template to recursively test the project:

```yaml
test:
  extends: .bazelisk
```

## Variables

The template has some environment variables that can customise the job behaviour.

### `ROOT`

To run `bazelisk` in multiple root directories:

```yaml
test:
  extends: .bazelisk
  parallel:
    matrix:
      - ROOT:
          - .
          - e2e
```

### `USE_BAZEL_VERSION`

`bazelisk` will download a version of Bazel as described in the [documentation][bazelisk-download].

To run against multiple versions of Bazel:

```yaml
test:
  extends: .bazelisk
  parallel:
    matrix:
      - USE_BAZEL_VERSION:
          - 6.3.1
          - 7.0.0
```

## Open Container Image (OCI)

The default image has `bazelisk` available on `PATH` and a minimal set of core utilities to satisfy GitLab CI job bring-up and built-in Bazel scripts.

The intention is that this image helps prove the hermeticity of a Bazel build as all tools will need to be resolved hermetically.

To use a different base image, override the image input for the component:

```yaml
include:
  - component: gitlab.arm.com/ci/component/bazelisk@<version>
    inputs:
      image: host.com/some/registry/bazelisk
      tag: latest
```

### OCI Local Usage

Assuming the current working directory is the root of the project to test, run the following:

```shell
$ podman run \
    -v $PWD:/mnt/src \
    -w /mnt/src \
    --rm \
    -it registry.gitlab.arm.com/ci/component/bazelisk:1.0.0-beta.2 \
    /bin/sh
sh-5.2$ bazelisk test //...
```

## YAML References

The `.bazelisk` template provides a simple way to invoke `bazelisk` in a job.

GitLab CI has a [`!reference`][gitlab-ci-reference] feature that allows parts of a template to be re-used in a fine grained way.

It is recommended to use this feature to build up a custom `bazelisk` job by using `!reference` to select parts of the `.bazelisk` image that are relevant.

Commonly, this is used to customise the `script` section of a job but still re-using parts of the `.bazelisk` template script:

```yaml
custom:
  extends:
    - .bazelisk
    - .credentials
  before_script:
    - !reference [.bazelisk, before_script]
    - !reference [.credentials, before_script]
  script:
    - bazelisk build //...
    - bazelisk run //...
```

[package-registry]: https://gitlab.arm.com/ci/component/bazelisk/-/packages
[gitlab-ci-reference]: https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#reference-tags
[bazelisk-download]: https://github.com/bazelbuild/bazelisk#how-does-bazelisk-know-which-bazel-version-to-run
