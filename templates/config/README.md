# `config`

> A set of jobs for testing different [Bazel configurations]

## Getting Started

```yaml
include:
  - component: "${CI_SERVER_HOST}/ci/component/bazelisk/config@<version>"
```

Select a `<version>` according to [CI component versions].

## Requirements

- There are `local`/`remote` configurations in `.bazelrc.ci`
- The `e2e` directory exists for end-to-end testing of the ruleset

[Bazel configurations]: https://bazel.build/extending/config
[CI component versions]: https://docs.gitlab.com/ee/ci/components/#component-versions
