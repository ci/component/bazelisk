# `version`

> A set of jobs for testing different Bazel versions

## Getting Started

```yaml
include:
  - component: "${CI_SERVER_HOST}/ci/component/bazelisk/version@<version>"
```

Select a `<version>` according to [CI component versions].

## Requirements

- The `e2e` directory exists for end-to-end testing of the ruleset

[CI component versions]: https://docs.gitlab.com/ee/ci/components/#component-versions
