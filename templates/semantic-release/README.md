# `semantic-release`

> A set of jobs for releasing a Bazel ruleset

## Getting Started

```yaml
include:
  - component: "${CI_SERVER_HOST}/ci/component/bazelisk/semantic-release@<version>"
```

Select a `<version>` according to [CI component versions].

## Requirements

- `semantic-release` is configured for the project

[CI component versions]: https://docs.gitlab.com/ee/ci/components/#component-versions
