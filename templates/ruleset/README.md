# `ruleset`

> An opinionated set of jobs for testing and releasing a Bazel ruleset

## Getting Started

```yaml
include:
  - component: "${CI_SERVER_HOST}/ci/component/bazelisk/ruleset@<version>"
```

Select a `<version>` according to [CI component versions].

## Requirements

- There are `local`/`remote` configurations in `.bazelrc.ci`
- The `e2e` directory exists for end-to-end testing of the ruleset
- `semantic-release` is configured for the project

[CI component versions]: https://docs.gitlab.com/ee/ci/components/#component-versions
