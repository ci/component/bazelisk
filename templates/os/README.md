# `os`

> A set of jobs for testing different operating systems

## Getting Started

```yaml
include:
  - component: "${CI_SERVER_HOST}/ci/component/bazelisk/os@<version>"
```

Select a `<version>` according to [CI component versions].

## Requirements

- The `e2e` directory exists for end-to-end testing of the ruleset
- The runner tags `linux`, `windows` and `macos` exist

[CI component versions]: https://docs.gitlab.com/ee/ci/components/#component-versions
