# `ci/component/bazelisk`

> A set of GitLab CI components for running Bazel builds

## Getting Started

Most users should choose between the [`ruleset`] (opinionated) and the [`bazelisk`] (fully-customizable) components.

The [`config`], [`version`], [`os`] and [`semantic-release`] are subcomponents of [`ruleset`], and are exposed for users wanting to opt-out of any of the default jobs.

[`ruleset`]: templates/ruleset/
[`bazelisk`]: templates/bazelisk/
[`config`]: templates/config/
[`version`]: templates/version/
[`os`]: templates/os/
[`semantic-release`]: templates/semantic-release/
