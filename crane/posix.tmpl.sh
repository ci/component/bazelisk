#!/bin/sh

# e: quit on command errors
# u: quit on undefined variables
set -eu

# Bazel substitutions
EXECUTABLE="{{path}}"
readonly EXECUTABLE

# Validate the executable exists
if ! test -f "${EXECUTABLE}"; then
  echo >&2 "Not found: ${EXECUTABLE}"
  exit 111
fi

# Validate the executable is...executable
if ! test -x "${EXECUTABLE}"; then
  echo >&2 "Not executable: ${EXECUTABLE}"
  exit 69
fi

# Pass on the argument to the actual executable
"${EXECUTABLE}" "${@}"
