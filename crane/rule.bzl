ATTRS = {
    "template": attr.label(
        doc = "The executable shell script to template",
        default = ":posix.tmpl.sh",
        allow_single_file = True,
    ),
}

def implementation(ctx):
    crane = ctx.toolchains["@rules_oci//oci:crane_toolchain_type"]

    executable = ctx.actions.declare_file("{}.sh".format(ctx.label.name))

    substitutions = {
        "{{path}}": crane.crane_info.binary.short_path,
    }

    ctx.actions.expand_template(
        template = ctx.file.template,
        output = executable,
        is_executable = True,
        substitutions = substitutions,
    )

    runfiles = crane.default.default_runfiles

    return DefaultInfo(executable = executable, runfiles = runfiles)

crane = rule(
    doc = "Creates a executable around the resolved `crane` binary.",
    implementation = implementation,
    attrs = ATTRS,
    executable = True,
    toolchains = [
        "@rules_oci//oci:crane_toolchain_type",
    ],
)
