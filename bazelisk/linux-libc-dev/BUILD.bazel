load("@bazel_skylib//lib:selects.bzl", "selects")
load("@rules_pkg//pkg:tar.bzl", "pkg_tar")

selects.config_setting_group(
    name = "arm64-linux",
    match_all = [
        "@platforms//cpu:arm64",
        "@platforms//os:linux",
    ],
)

selects.config_setting_group(
    name = "amd64-linux",
    match_all = [
        "@platforms//cpu:x86_64",
        "@platforms//os:linux",
    ],
)

alias(
    name = "linux-libc-dev",
    actual = select(
        {
            ":arm64-linux": "@linux-libc-dev-arm64//:usr/include/linux/version.h",
            ":amd64-linux": "@linux-libc-dev-amd64//:usr/include/linux/version.h",
        },
        no_match_error = "No `linux-libc-dev` downloaded for this platform.",
    ),
)

pkg_tar(
    name = "tar",
    srcs = [
        ":linux-libc-dev",
    ],
    mode = "0644",
    package_dir = select(
        {
            ":arm64-linux": "/usr/include/linux",
            ":amd64-linux": "/usr/include/linux",
        },
        no_match_error = "No `linux-libc-dev` package directory.",
    ),
    visibility = ["//bazelisk:__subpackages__"],
)
