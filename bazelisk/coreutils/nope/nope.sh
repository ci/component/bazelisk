#!/bin/sh

set -eu
cat <<EOF >&2
${0} ${*}

Resolve \`${0##*/}\` hermetically rather than using this local stub.

The \`bazelisk\` container provides stubs for common Unix facilities such as
\`${0##*/}\` that fail by default.

This indicates that the current Bazel build is not hermetic, i.e. it is not
resolving tooling in a reproducible way across systems.

To resolve this error, make sure that the \`${0##*/}\` is resolved by
toolchain resolution to a downloaded, prebuilt binary _or_ uses a binary built
with Bazel itself.
EOF

exit 123
