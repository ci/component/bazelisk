#!/usr/bin/env bash

set -euo pipefail

declare -a ARGS
for ARG in "${@}"; do
  case "${ARG}" in
  "--mime-type")
    MIME=1
    readonly MIME
    ;;
  *)
    ARGS+=("${ARG}")
    ;;
  esac
done

if test "${MIME-0}" = 0; then
  toybox file "${ARGS[@]}"
  exit 0
fi

toybox file "${ARGS[@]}" |
  while IFS=":" read -r FIRST SECOND; do
    # Support brief output (`-b`)
    if test -z "${SECOND}"; then
      PREFIX=""
      CATEGORY="${FIRST}"
    else
      SPACE="${SECOND%%[![:space:]]*}"
      CATEGORY="${SECOND#"${SPACE}"}"
      PREFIX="${FIRST}:${SPACE}"
    fi

    # Do MIME conversion
    # TODO: support all in https://github.com/landley/toybox/blob/master/toys/posix/file.c
    case "${CATEGORY}" in
    "cannot open "*)
      KIND="${CATEGORY}"
      ;;
    "empty")
      KIND="inode/x-empty"
      ;;
    "directory")
      KIND="inode/directory"
      ;;
    "fifo")
      KIND="inode/fifo"
      ;;
    "socket")
      KIND="inode/socket"
      ;;
    "symbolic link to "*)
      KIND="inode/symlink"
      ;;
    "character special ("*"/"*")")
      KIND="inode/chardevice"
      ;;
    "block special ("*"/"*")")
      KIND="inode/blockdevice"
      ;;
    "ASCII text")
      KIND="text/plain"
      ;;
    *" script")
      KIND="text/x-shellscript"
      ;;
    "ar archive")
      KIND="text/x-archive"
      ;;
    "ELF executable")
      KIND="application/x-executable"
      ;;
    "ELF shared object"*)
      KIND="application/x-sharedlib"
      ;;
    *)
      KIND="application/octet-stream"
      ;;
    esac

    printf "%s%s\n" "${PREFIX}" "${KIND}"
  done
