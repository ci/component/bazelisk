# Contributing

## Getting Started

- [Install](https://github.com/bazelbuild/bazelisk#installation) `bazelisk`
- Run `bazelisk test //...`
- Run `bazelisk test $(bazelisk query "attr(tags, '\bmanual\b', //...)")`
